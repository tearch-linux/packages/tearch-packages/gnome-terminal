# Maintainer: Muhammed Efe Çetin <efectn@protonmail.com>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>

pkgname=gnome-terminal
pkgver=3.42.2
pkgrel=2
pkgdesc="The GNOME Terminal Emulator"
url="https://wiki.gnome.org/Apps/Terminal"
arch=(x86_64)
license=(GPL)
depends=(vte3 gsettings-desktop-schemas)
makedepends=(docbook-xsl libnautilus-extension gnome-shell yelp-tools git meson)
groups=(gnome)
_commit=f300eed28e6755458ebaaecd9a8fe5f6b4ca29cd  # tags/3.42.2^0
source=("git+https://gitlab.gnome.org/GNOME/gnome-terminal.git#commit=$_commit" "0001-gnome-terminal-always-headerbar.patch" "0002-gnome-terminal-transparency.patch")
sha256sums=('SKIP' '26f69272d6d0cd0b602de299969b852a44473271c0da869d468a072c89434e2a' '8e7c72f0bad30d4a2ba98acd14aad956731ca0bc3521029fc2e1858a28ee108d')

pkgver() {
  cd $pkgname
  git describe --tags | sed 's/-/+/g'
}

prepare() {
  cd $pkgname
  
  # Fix build with meson 0.61
  # https://bugs.archlinux.org/task/73356
  git cherry-pick -n 9a168cc23962ce9fa106dc8a40407d381a3db403

  patch -Np1 -i ../0001-gnome-terminal-always-headerbar.patch
  patch -Np1 -i ../0002-gnome-terminal-transparency.patch
}

build() {
  arch-meson $pkgname build
  meson compile -C build
}

check() {
  meson test -C build --print-errorlogs
}

package() {
  meson install -C build --destdir "$pkgdir"
}